function [C, sigma] = dataset3Params(X, y, Xval, yval)
%DATASET3PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = DATASET3PARAMS(X, y, Xval, yval) returns your choice of C and
%   sigma. You should complete this function to return the optimal C and
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
C = 1;
sigma = 0.3;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example,
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using
%        mean(double(predictions ~= yval))
%
Cl = [0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30];
Sl = [0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30];

for i = 1:size(Cl, 2),
    C = Cl(i);
    for j = 1:size(Sl, 2),
        sigma = Sl(j);
        model= svmTrain(X, y, C, @(x1, x2) gaussianKernel(x1, x2, sigma));
        p = svmPredict(model, Xval);
        pe(i, j) = mean(double(p ~= yval));
    end
end

v = min(pe(:));
[i, j] = find(pe == v);

C = Cl(i);
sigma = Sl(j);

% =========================================================================

end
